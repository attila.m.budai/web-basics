import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Todo} from "./Todo";
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  private todoArray: Todo[];

  constructor(private http: HttpClient, private router: Router) {
  }

  public ngOnInit(): void {
    this.refreshData();
  }

  public refreshData(): void {
    this.http.get<Todo[]>("http://localhost:8080/api/todo/list")
      .subscribe(
        data => {
          this.todoArray = data;
          console.log(this.todoArray);
          this.router.navigateByUrl("/", {skipLocationChange: true});
        },
        err => {
          console.error(err);
          alert(err.message);
        });
  }

  public addTodo(text: string): void {
    if (text !== "") {
      let todo = new Todo(undefined, text, false);
      this.http.post("http://localhost:8080/api/todo/add", todo, {responseType: 'text'})
        .subscribe(
          response => {
            if (!response.includes("Saved")) {
              console.error(response);
              alert("Save failed!");
            } else {
              this.refreshData();
            }
          },
          err => {
            console.error(err);
            alert("Save failed!");
          }
        )
    }
  }

  public todoSubmit(formValue: any): void {
    if (formValue !== "") {
      this.addTodo(formValue.todo);
    }
  }

  public deleteTodo(todo: Todo): void {
    this.http.delete(`http://localhost:8080/api/todo/remove-by-id?id=${todo.id}`)
      .subscribe(
        response => this.refreshData(),
        err => {
          console.error(err);
          alert(err);
        }
      );
  }

  public todoFinished(todo: Todo, event: any): void {
    todo.completed = event.target.checked;
  }

}
