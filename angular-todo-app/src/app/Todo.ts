import * as uuid from 'uuid';

export class Todo {
  constructor(
    public id: string = uuid.v4(),
    public text: string,
    public completed: boolean
  ) {
  }
}
