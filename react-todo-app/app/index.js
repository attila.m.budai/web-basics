import React from 'react';
import ReactDOM from 'react-dom';
import AddTodo from "./containers/AddTodo";
import TodoList from "./containers/TodoList";

class App extends React.Component {

    constructor(props) {
        super(props);
        this.refreshCallbackHolder = {refreshCallback: () => console.error('Callback not specified!')}
    }

    render() {
        return (
            <div>
                <AddTodo refreshCallbackHolder={this.refreshCallbackHolder}/>
                <TodoList refreshCallbackHolder={this.refreshCallbackHolder}/>
            </div>
        )
    }
}

ReactDOM.render(<App />, document.getElementById('root'));