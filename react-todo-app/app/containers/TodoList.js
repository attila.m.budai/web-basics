import React, { Component } from 'react';
import axios from 'axios';
import TodoRow from '../components/TodoRow'

export default class TodoList extends Component {

    constructor(props) {
        super(props);
        this.state = { todos: [] };

        this.props.refreshCallbackHolder.refreshCallback = this.queryTodos.bind(this);
    }

    queryTodos() {
        axios.get("http://localhost:8080/api/todo/list")
            .then(response => {
                this.setState({ todos: response.data });
                console.log(this.state.todos);
            })
            .catch(error => {
                console.error(error);
                alert("Server response error: " + error.toString());
            });
    }


    componentWillMount() {
        this.queryTodos();
    }

    render() {
        return (
            <table className="table table-hover">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Text</th>
                    <th>Completed</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                {
                    this.state.todos
                        .map(todo =>
                            <TodoRow key={todo.id} todo={todo}
                                     refreshCallbackHolder={this.props.refreshCallbackHolder}/>
                        )
                }
                </tbody>
            </table>
        );
    }

}