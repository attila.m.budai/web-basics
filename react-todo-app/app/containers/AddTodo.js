import React, { Component } from 'react';
import axios from 'axios';
import uuid from 'uuid';

export default class AddTodo extends Component {

    constructor(props) {
        super(props);

        this.state = { text: '' };

        this.onInputChange = this.onInputChange.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);
    }

    onInputChange(event) {
        this.setState({ text: event.target.value })
    }

    onFormSubmit(event) {
        event.preventDefault();
        axios.post("http://localhost:8080/api/todo/add", {id: uuid.v4(), text: this.state.text, completed: false})
            .then(response => {
                if (response.data && response.data.includes('Saved')) {
                    this.props.refreshCallbackHolder.refreshCallback();
                } else {
                    console.error(response);
                    alert('Error during save: ' + response.toString());
                }
            })
            .catch(err => {
                console.error(err);
                alert('Error during save: ' + err.toString());
            });
        this.setState({ text: '' });
    }

    render() {
        return (
            <form onSubmit={this.onFormSubmit} className="input-group">
                <input
                    placeholder="Itt add meg az új tudo szövegét"
                    className="form-control"
                    value={this.state.text}
                    onChange={this.onInputChange}/>
                <span className="input-group-btn">
                    <button type="submit" className="btn btn-secondary">Add</button>
                </span>
            </form>
        );
    }

}