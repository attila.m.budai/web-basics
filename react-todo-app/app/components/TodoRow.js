import React, { Component } from 'react';
import axios from 'axios';

export default class TodoRow extends Component {

    constructor(props) {
        super(props);

        this.removeClicked = this.removeClicked.bind(this);
    }

    removeClicked(event) {
        axios.delete(`http://localhost:8080/api/todo/remove-by-id?id=${this.props.todo.id}`)
            .then(response => {
                this.props.refreshCallbackHolder.refreshCallback();
            })
            .catch(err => {
                console.error(err);
                alert('Error during delete: ' + err.toString());
            });
    }

    checkedChanged(event) {
        console.log(event);
    }

    render() {
        return (
            <tr>
                <td>{this.props.todo.id}</td>
                <td>{this.props.todo.text}</td>
                <td>
                    <div className="checkbox">
                        <label className="checkbox-label">
                            <input type="checkbox" value="" checked={this.props.todo.completed}
                                   onChange={this.checkedChanged} />
                            <span className="cr"><i className="cr-icon fa fa-check"/></span>
                        </label>
                    </div>
                </td>
                <td>
                    <button type="button" className="btn btn-danger"
                            onClick={this.removeClicked}>
                        <i className="fa fa-trash" aria-hidden="true"/>
                    </button>
                </td>
            </tr>
        );
    }

}