package hu.webvalto.examples.longpolling;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class Main {

    private static String newMessageToSend = "";

    public static void main(String[] args) {
        try {
            startInputScannerThread();
            HttpServer httpServer = HttpServer.create(new InetSocketAddress(9998), 0);
            httpServer.createContext("/", Main::handleIndex);
            httpServer.createContext("/messages", Main::handleMessages);
            httpServer.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void startInputScannerThread() {
        new Thread(() -> {
            Scanner scanner = new Scanner(System.in);
            while (scanner.hasNextLine()) {
                newMessageToSend = scanner.nextLine();
            }
        }).start();
    }

    private static void handleIndex(HttpExchange exchange) {
        try {
            Scanner scanner = new Scanner(Object.class.getResourceAsStream("/index.html")).useDelimiter("\\A");
            String content = scanner.next();
            byte[] contentBytes = content.getBytes(StandardCharsets.UTF_8);
            writeResponse(exchange, contentBytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void writeResponse(HttpExchange exchange, byte[] contentBytes) throws IOException {
        exchange.sendResponseHeaders(200, contentBytes.length);
        exchange.getResponseBody().write(contentBytes);
        exchange.getResponseBody().close();
    }

    private static void handleMessages(HttpExchange exchange) {
        try {
            while (newMessageToSend.equals("")) {
                Thread.sleep(200);
            }
            byte[] messageBytes = newMessageToSend.getBytes(StandardCharsets.UTF_8);
            writeResponse(exchange, messageBytes);
            newMessageToSend = "";
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
    }

}
