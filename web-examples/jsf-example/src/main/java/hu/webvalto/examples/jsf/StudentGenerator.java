package hu.webvalto.examples.jsf;

import java.time.LocalDate;
import java.time.YearMonth;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StudentGenerator {

    private static final String consonants = "bcdfghjklmnpqrstvwxyz";
    private static final String vowels = "aeiouöüóőúéáűí";

    private final java.util.Random rand = new java.util.Random();

    private boolean startWithVowel = false;

    public static List<Student> generateStudents(int size) {
        StudentGenerator generator = new StudentGenerator();
        return generator.createNames(size)
            .map(name -> new Student(UUID.randomUUID().toString(), name, generator.randomDate()))
            .collect(Collectors.toList());
    }

    private LocalDate randomDate() {
        int year = rand.nextInt(118) + 1900;
        int month = rand.nextInt(11) + 1;
        LocalDate date = YearMonth.of(year, month).atEndOfMonth();
        int dayOfMonth = rand.nextInt(date.getDayOfMonth() - 1) + 1;
        return date.withDayOfMonth(dayOfMonth);
    }

    private Stream<String> createNames(int size) {
        return IntStream.range(0, size)
            .boxed()
            .map(i -> randomName());
    }

    private String randomName() {
        return IntStream.range(0, rand.nextInt(2) + 2)
            .boxed()
            .map(i -> randomNamePart())
            .collect(Collectors.joining(" "));
    }

    private String randomNamePart() {
        startWithVowel = rand.nextBoolean();
        StringBuilder builder = new StringBuilder();
        int length = rand.nextInt(3) + 5;
        IntStream.range(0, length)
            .boxed()
            .map(this::chooseOneChar)
            .forEach(builder::append);
        return builder.toString();
    }

    private Character chooseOneChar(int i) {
        char res = (startWithVowel && i % 2 == 1) || (!startWithVowel && i % 2 == 0)
            ? consonants.charAt(rand.nextInt(consonants.length()))
            : vowels.charAt(rand.nextInt(vowels.length()));
        return i == 0 ? Character.toUpperCase(res) : res;
    }

}
