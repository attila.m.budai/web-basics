package hu.webvalto.examples.jsf;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.List;

@SessionScoped
@ManagedBean(name = "studentStoreService")
public class StudentStoreService {

    private List<Student> students;

    @PostConstruct
    public void generateStudents() {
        students = StudentGenerator.generateStudents(100);
    }

    public List<Student> getStudents() {
        return students;
    }
}
