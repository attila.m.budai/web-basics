package hu.webvalto.examples.jsf;

import org.primefaces.PrimeFaces;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@ViewScoped
@ManagedBean(name = "studentsBean")
public class StudentsBean implements Serializable {

    @ManagedProperty("#{studentStoreService}")
    private StudentStoreService studentStore;

    private Student newStudent = new Student();

    public void saveStudent() {
        studentStore.getStudents().add(newStudent);
        newStudent = new Student();
        PrimeFaces.current().ajax().update("form");
    }

    public void setNewStudentDateOfBirth(Date date) {
        if (date != null) {
            newStudent.setDateOfBirth(
                date.toInstant()
                    .atZone(ZoneId.systemDefault())
                    .toLocalDate()
            );
        }
    }

    public Date getNewStudentDateOfBirth() {
        if (newStudent.getDateOfBirth() != null) {
            return Date.from(
                newStudent.getDateOfBirth()
                    .atStartOfDay()
                    .atZone(ZoneId.systemDefault())
                    .toInstant()
            );
        }
        return null;
    }

    public List<Student> getStudents() {
        return studentStore.getStudents();
    }

    public void setStudentStore(StudentStoreService studentStore) {
        this.studentStore = studentStore;
    }

    public Student getNewStudent() {
        return newStudent;
    }

    public void setNewStudent(Student newStudent) {
        this.newStudent = newStudent;
    }
}
