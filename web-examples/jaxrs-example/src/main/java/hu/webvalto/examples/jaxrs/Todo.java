package hu.webvalto.examples.jaxrs;

import java.util.UUID;

public class Todo {

    private UUID id;

    private String text;

    private boolean completed;

    public Todo() {
        this.id = UUID.randomUUID();
    }

    public Todo(String text, boolean completed) {
        this();
        this.text = text;
        this.completed = completed;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }
}
