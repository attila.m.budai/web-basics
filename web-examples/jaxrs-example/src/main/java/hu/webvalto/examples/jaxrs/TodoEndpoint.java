package hu.webvalto.examples.jaxrs;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Component
@Path("/todo")
@Api("todo")
public class TodoEndpoint {

    private TodoContainerService todoContainer;

    @Autowired
    public TodoEndpoint(TodoContainerService todoContainer) {
        this.todoContainer = todoContainer;
    }

    @GET
    @Path("/list")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Todo> listTodos() {
        return todoContainer.getTodoList();
    }

    @DELETE
    @Path("/remove")
    @Consumes(MediaType.APPLICATION_JSON)
    public void removeTodo(Todo todo) {
        todoContainer.removeTodo(todo);
    }

    @DELETE
    @Path("/remove-by-id")
    public void removeTodoById(@QueryParam("id") @NotNull String id) {
        todoContainer.getTodoList().stream()
            .filter(todo -> todo.getId().toString().equals(id))
            .findAny()
            .ifPresent(todoContainer::removeTodo);
    }

    @POST
    @Path("/add")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addTodo(Todo todo) {
        if (todo.getId() == null) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity("Todo without id cannot be saved")
                .build();
        }

        try {
            todoContainer.addTodo(todo);
        } catch (IllegalArgumentException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(e.getMessage())
                .build();
        }

        return Response.status(Response.Status.OK)
            .entity("Saved")
            .build();
    }

}
