package hu.webvalto.examples.jaxrs;

import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

@Service
public class TodoContainerService {

    private List<Todo> listOfTodos;

    public TodoContainerService() {
        listOfTodos = new ArrayList<>();
    }

    public List<Todo> getTodoList() {
        return Collections.unmodifiableList(listOfTodos);
    }

    @PostConstruct
    public void init() {
        IntStream.range(1, 6)
            .boxed()
            .map(i -> new Todo("Task " + i, i % 2 == 0))
            .forEach(listOfTodos::add);
    }

    public void removeTodo(Todo toRemove) {
        listOfTodos.stream()
            .filter(todo -> todo.getId().equals(toRemove.getId()))
            .findAny()
            .ifPresent(todo -> listOfTodos.remove(todo));
    }

    public void addTodo(Todo toAdd) {
        if (listOfTodos.stream().anyMatch(todo -> todo.getId().equals(toAdd.getId()))) {
            throw new IllegalArgumentException("Todo with the same id, is already present!");
        }
        listOfTodos.add(toAdd);
    }

}
