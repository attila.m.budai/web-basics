package hu.webvalto.example.urlpost;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        try {
            URL url = new URL("https://ptsv2.com/t/t9z0k-1547201034/post");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty( "Content-type", "plain/text");
            connection.setRequestProperty( "Accept", "*/*" );
            PrintWriter writer = new PrintWriter(connection.getOutputStream());
            writer.println("some test string");
            writer.flush();

            Scanner scanner = new Scanner(connection.getInputStream());
            while (scanner.hasNextLine()) {
                System.out.println(scanner.nextLine());
            }

            writer.close();
            scanner.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
