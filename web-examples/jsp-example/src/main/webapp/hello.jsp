<%@ page import="java.time.LocalDateTime" %>
<%@ page import="static java.time.temporal.ChronoField.*" %>
<%@ page pageEncoding="UTF-8" %>
<html lang="hu">
<head>
    <meta charset="UTF-8">
</head>
<body>
<% if (request.getParameterMap().containsKey("name")) { %>
    <h1>Hello <%= request.getParameter("name") %>!</h1>
<% } else { %>
    <%@include file="error.jsp" %>
<% } %>

<%! private LocalDateTime currentDateTime = LocalDateTime.now(); %>
<h4 style="text-align: center">
    Aktuális dátum: <%= currentDateTime.toLocalDate() %>
    <br/>
    Aktuális idő: <%=
        currentDateTime.toLocalTime().get(HOUR_OF_DAY)
        + ":" + currentDateTime.toLocalTime().get(MINUTE_OF_HOUR) %>
</h4>
</body>
</html>