package hu.webvalto.examples.jsp;

import org.apache.catalina.LifecycleException;
import org.apache.catalina.startup.Tomcat;

import java.io.File;

public class Main {

    public static void main(String[] args) {
        Tomcat tomcat = new Tomcat();
        tomcat.setPort(8099);
        tomcat.getConnector();
        String moduleBase = "web-examples/jsp-example";
        tomcat.addWebapp("/", new File(moduleBase + "/src/main/webapp").getAbsolutePath());
        try {
            tomcat.start();
        } catch (LifecycleException e) {
            e.printStackTrace();
        }
        tomcat.getServer().await();
    }

}
