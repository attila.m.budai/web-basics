package hu.webvalto.examples.ajaxexample;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import java.util.UUID;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        try {
            HttpServer httpServer = HttpServer.create(new InetSocketAddress(9998), 0);
            httpServer.createContext("/", Main::handleIndex);
            httpServer.createContext("/generate", Main::handleGenerate);
            httpServer.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void handleIndex(HttpExchange exchange) {
        try {
            Scanner scanner = new Scanner(Object.class.getResourceAsStream("/index.html")).useDelimiter("\\A");
            byte[] response = scanner.next().getBytes(StandardCharsets.UTF_8);
            exchange.sendResponseHeaders(200, response.length);
            exchange.getResponseBody().write(response);
            exchange.getResponseBody().flush();
            exchange.getResponseBody().close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void handleGenerate(HttpExchange exchange) {
        try (OutputStream responseBody = exchange.getResponseBody()) {
            String requestBody = new BufferedReader(new InputStreamReader(exchange.getRequestBody()))
                .lines()
                .collect(Collectors.joining());
            System.out.println(requestBody);
            boolean upperCase = requestBody.contains("true");
            String response = UUID.randomUUID().toString();
            response = upperCase ? response.toUpperCase() : response;
            exchange.sendResponseHeaders(200, response.getBytes(StandardCharsets.UTF_8).length);
            responseBody.write(response.getBytes(StandardCharsets.UTF_8));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
