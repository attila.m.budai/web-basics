package hu.webvalto.example.tcpchat.websocket;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@ServerEndpoint("/chat")
public class ChatWebSocket {

    private static List<Session> sessions = new ArrayList<>();

    @OnOpen
    public void onOpen(Session session) {
        System.out.println("Session start, id: " + session.getId());
        sessions.add(session);
    }

    @OnMessage
    public void onMessage(String message, Session sess) {
        System.out.println("New message from: " + sess.getId() + ": " + message);
        sessions.stream()
            .map(Session::getBasicRemote)
            .forEach(basic -> {
                try {
                    basic.sendText(message);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
    }

    @OnClose
    public void onClose(Session session) {
        System.out.println("Session closed, id: " + session.getId());
        sessions.remove(session);
    }

}
