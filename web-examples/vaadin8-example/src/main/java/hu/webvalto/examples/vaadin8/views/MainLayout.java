package hu.webvalto.examples.vaadin8.views;

import com.vaadin.event.ShortcutAction;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.*;
import com.vaadin.ui.renderers.ComponentRenderer;
import com.vaadin.ui.themes.ValoTheme;
import hu.webvalto.examples.vaadin8.entities.Todo;
import hu.webvalto.examples.vaadin8.services.TodoService;

public class MainLayout extends VerticalLayout {

    private final TodoService todoService;

    private TextField addTodoTextField;
    private Grid<Todo> todoListGrid;

    public MainLayout(TodoService todoService) {
        this.todoService = todoService;

        VerticalLayout mainContainer = setupMainContainer();
        setupAppHeader(mainContainer);
        setupAddTodoBar(mainContainer);
        setupListOfTodosLabel(mainContainer);
        setupTodoListGrid(todoService, mainContainer);
        setupDeleteButton(mainContainer);

        this.addComponent(mainContainer);
        this.setComponentAlignment(mainContainer, Alignment.MIDDLE_CENTER);

        this.setMargin(new MarginInfo(true, false));
        this.setSpacing(false);
    }

    private void setupListOfTodosLabel(VerticalLayout mainContainer) {
        Label listOfTodosLabel = new Label("List of Todos:");
        listOfTodosLabel.setStyleName(ValoTheme.LABEL_H3);
        mainContainer.addComponent(listOfTodosLabel);
    }

    private void setupAppHeader(VerticalLayout mainContainer) {
        Label appHeaderLabel = new Label("TODO App");
        appHeaderLabel.setStyleName(ValoTheme.LABEL_H1);
        mainContainer.addComponent(appHeaderLabel);
        mainContainer.setComponentAlignment(appHeaderLabel, Alignment.MIDDLE_CENTER);
    }

    private void setupDeleteButton(VerticalLayout mainContainer) {
        Button deleteButton = new Button(VaadinIcons.TRASH, this::deleteSelected);
        deleteButton.setStyleName(ValoTheme.BUTTON_DANGER);

        mainContainer.addComponent(deleteButton);
        mainContainer.setComponentAlignment(deleteButton, Alignment.MIDDLE_RIGHT);
        mainContainer.setExpandRatio(deleteButton, 0.0f);
    }

    private void setupTodoListGrid(TodoService todoService, VerticalLayout mainContainer) {
        todoListGrid = new Grid<>();
        todoListGrid.addColumn(Todo::getId)
            .setCaption("Id")
            .setResizable(true);

        todoListGrid.addColumn(Todo::getText)
            .setCaption("Text")
            .setResizable(true);

        todoListGrid.addColumn(todo -> {
            CheckBox completedCheckBox = new CheckBox();
            completedCheckBox.setValue(todo.isCompleted());
            completedCheckBox.addValueChangeListener(event -> todo.setCompleted(event.getValue()));
            return completedCheckBox;
        }, new ComponentRenderer())
        .setCaption("Completed")
        .setSortable(false);

        todoListGrid.setSelectionMode(Grid.SelectionMode.MULTI);
        todoListGrid.setSizeFull();
        todoListGrid.setItems(todoService.getTodoList());

        mainContainer.addComponent(todoListGrid);
        mainContainer.setExpandRatio(todoListGrid, 1.0f);
    }

    private void setupAddTodoBar(VerticalLayout mainContainer) {
        HorizontalLayout addTodoBar = new HorizontalLayout();
        addTodoBar.setWidth(100, Unit.PERCENTAGE);
        addTodoBar.setSpacing(false);
        addTodoBar.setMargin(false);

        addTodoTextField = new TextField();
        addTodoTextField.setWidth(100, Unit.PERCENTAGE);
        addTodoTextField.setPlaceholder("Ide add meg az új tudu szövegét");

        addTodoBar.addComponent(addTodoTextField);
        addTodoBar.setExpandRatio(addTodoTextField, 0.9f);

        Button addButton = new Button(VaadinIcons.PLUS);
        addButton.setWidth(100, Unit.PERCENTAGE);
        addButton.addClickListener(this::addTodo);
        addButton.setStyleName(ValoTheme.BUTTON_PRIMARY);
        addButton.setClickShortcut(ShortcutAction.KeyCode.ENTER);

        addTodoBar.addComponent(addButton);
        addTodoBar.setExpandRatio(addButton, 0.1f);

        mainContainer.addComponent(addTodoBar);
        mainContainer.setExpandRatio(addTodoBar, 0.0f);
    }

    private VerticalLayout setupMainContainer() {
        VerticalLayout mainContainer = new VerticalLayout();
        mainContainer.setWidth(85, Unit.PERCENTAGE);
        mainContainer.setHeight(100, Unit.PERCENTAGE);
        mainContainer.setSpacing(true);
        mainContainer.setMargin(false);
        return mainContainer;
    }

    private void addTodo(Button.ClickEvent event) {
        if (!addTodoTextField.isEmpty()) {
            Todo todo = new Todo(addTodoTextField.getValue(), false);
            todoService.getTodoList().add(todo);
            todoListGrid.getDataProvider().refreshAll();
            addTodoTextField.setValue("");
        }
    }

    private void deleteSelected(Button.ClickEvent event) {
        if (!todoListGrid.getSelectedItems().isEmpty()) {
            todoService.getTodoList().removeAll(todoListGrid.getSelectedItems());
            todoListGrid.getDataProvider().refreshAll();
        }
    }

}
