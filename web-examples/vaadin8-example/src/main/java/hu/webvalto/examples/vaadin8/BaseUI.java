package hu.webvalto.examples.vaadin8;

import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.UI;
import hu.webvalto.examples.vaadin8.services.TodoService;
import hu.webvalto.examples.vaadin8.views.MainLayout;
import org.springframework.beans.factory.annotation.Autowired;

@SpringUI
@Theme("valo")
public class BaseUI extends UI {

    private MainLayout layout;

    private final TodoService todoService;

    @Autowired
    public BaseUI(TodoService todoService) {
        this.todoService = todoService;
    }

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        layout = new MainLayout(todoService);
        layout.setSizeFull();
        this.setContent(layout);
    }
}
