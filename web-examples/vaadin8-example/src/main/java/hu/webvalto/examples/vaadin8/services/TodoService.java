package hu.webvalto.examples.vaadin8.services;

import hu.webvalto.examples.vaadin8.entities.Todo;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class TodoService {

    private List<Todo> todoList;

    @PostConstruct
    private void init() {
        todoList = IntStream.range(0, 10_000)
            .boxed()
            .map(i -> new Todo("Task " + i, i % 2 == 0))
            .collect(Collectors.toList());
    }

    public List<Todo> getTodoList() {
        return todoList;
    }

}
