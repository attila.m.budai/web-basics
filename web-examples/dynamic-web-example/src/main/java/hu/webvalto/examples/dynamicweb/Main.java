package hu.webvalto.examples.dynamicweb;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        try {
            HttpServer httpServer = HttpServer.create(new InetSocketAddress(9998), 0);
            httpServer.createContext("/", Main::handleIndex);
            httpServer.createContext("/hello", Main::handleHello);
            httpServer.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void handleIndex(HttpExchange exchange) {

    }

    private static void handleHello(HttpExchange exchange) {

    }

}
