package hu.webvalto.examples.uibuilder.ui;

import com.vaadin.flow.spring.annotation.VaadinSessionScope;
import hu.webvalto.examples.uibuilder.entities.Student;
import hu.webvalto.examples.uibuilder.repositories.StudentRepo;
import io.devbench.uibuilder.controllerbean.ControllerBean;
import io.devbench.uibuilder.crud.AbstractSpringControllerBean;
import org.springframework.beans.factory.annotation.Autowired;

@VaadinSessionScope
@ControllerBean("studentCB")
public class StudentControllerBean extends AbstractSpringControllerBean<Student, StudentRepo> {

    @Autowired
    public StudentControllerBean(StudentRepo studentRepo) {
        super(Student::new, studentRepo);
    }

}
