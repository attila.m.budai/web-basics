package hu.webvalto.examples.uibuilder.repositories;

import hu.webvalto.examples.uibuilder.entities.Student;
import io.devbench.uibuilder.data.annotations.TargetDataSource;
import org.springframework.data.ebean.repository.EbeanRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
@TargetDataSource(name = "studentsDS")
public interface StudentRepo extends EbeanRepository<Student, UUID> {
}
