package hu.webvalto.examples.uibuilder.ui;

import io.devbench.uibuilder.components.window.UIBuilderWindow;
import io.devbench.uibuilder.controllerbean.ControllerBean;
import io.devbench.uibuilder.controllerbean.UIComponent;
import io.devbench.uibuilder.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.flow.FlowManager;
import io.devbench.uibuilder.scope.page.PageScope;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.subject.Subject;

import java.util.Optional;

@PageScope("login-page")
@ControllerBean("loginController")
public class LoginControllerBean {

    private String username;
    private String password;

    public String getUserMenuName() {
        return Optional
            .ofNullable(SecurityUtils.getSubject())
            .map(Subject::getPrincipal)
            .map(Object::toString)
            .orElse("");
    }

    @UIEventHandler("login")
    public void login(@UIComponent("error-window") UIBuilderWindow errorWindow) {
        try {
            Subject subject = SecurityUtils.getSubject();
            subject.login(new UsernamePasswordToken(username, password));
            FlowManager.getCurrent().navigate("home");
            FlowManager.getCurrent().reload();
        } catch (AuthenticationException | AuthorizationException e) {
            e.printStackTrace();
            errorWindow.open();
        }
    }

    @UIEventHandler("errorClose")
    public void errorClose(@UIComponent("error-window") UIBuilderWindow window) {
        window.close();
    }

    @UIEventHandler("logout")
    public void logout() {
        SecurityUtils.getSubject().logout();
        FlowManager.getCurrent().reload();
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
