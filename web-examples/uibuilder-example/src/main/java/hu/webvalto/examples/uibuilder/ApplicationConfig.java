package hu.webvalto.examples.uibuilder;

import io.ebean.EbeanServer;
import io.ebean.EbeanServerFactory;
import io.ebean.config.ServerConfig;
import io.ebean.spring.txn.SpringJdbcTransactionManager;
import org.apache.shiro.authz.permission.WildcardPermission;
import org.apache.shiro.realm.SimpleAccountRealm;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.ebean.repository.config.EnableEbeanRepositories;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.Collections;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Configuration
@EnableEbeanRepositories
public class ApplicationConfig {

    public static final String ADMIN_ROLE = "ADMIN";
    public static final String USER_ROLE = "USER";

    public static final String LIST_PERMISSION = "list";
    public static final String EDIT_PERMISSION = "edit";

    @Bean
    public SimpleAccountRealm simpleAccountRealm() {
        SimpleAccountRealm simpleAccountRealm = new SimpleAccountRealm();
        simpleAccountRealm.addAccount("admin", "admin", ADMIN_ROLE);
        simpleAccountRealm.addAccount("user", "user", USER_ROLE);
        simpleAccountRealm.setRolePermissionResolver(roleString -> {
            if (ADMIN_ROLE.equals(roleString)) {
                return Stream.of(LIST_PERMISSION, EDIT_PERMISSION)
                    .map(WildcardPermission::new)
                    .collect(Collectors.toList());
            } else {
                return Collections.singleton(new WildcardPermission(LIST_PERMISSION));
            }
        });
        return simpleAccountRealm;
    }

    @Bean
    public DataSource dataSource() {
        return new EmbeddedDatabaseBuilder().setType(EmbeddedDatabaseType.H2).build();
    }

    @Bean
    public PlatformTransactionManager transactionManager(DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

    @Bean
    @Primary
    public ServerConfig defaultEbeanServerConfig(DataSource dataSource) {
        ServerConfig config = new ServerConfig();

        config.setDataSource(dataSource);
        config.setExternalTransactionManager(new SpringJdbcTransactionManager());

        config.loadFromProperties();
        config.setDefaultServer(true);
        config.setRegister(true);
        config.setAutoCommitMode(false);
        config.setExpressionNativeIlike(true);

        return config;
    }

    @Bean
    @Primary
    public EbeanServer defaultEbeanServer(ServerConfig defaultEbeanServerConfig) {
        return EbeanServerFactory.create(defaultEbeanServerConfig);
    }

}
