package hu.webvalto.examples.uibuilder.entities;

import org.springframework.data.domain.Persistable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Version;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.UUID;

@Entity
public class Student implements Persistable<UUID> {

    @Id
    private UUID id;

    @Version
    private Timestamp version;

    @Column
    private String name;

    @Column
    private LocalDate dateOfBirth;


    public Student() {
        id = UUID.randomUUID();
    }

    public Student(UUID id, String name, LocalDate dateOfBirth) {
        this.id = id;
        this.name = name;
        this.dateOfBirth = dateOfBirth;
    }


    public UUID getId() {
        return id;
    }

    @Override
    public boolean isNew() {
        return version == null;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Timestamp getVersion() {
        return version;
    }

    public void setVersion(Timestamp version) {
        this.version = version;
    }
}
