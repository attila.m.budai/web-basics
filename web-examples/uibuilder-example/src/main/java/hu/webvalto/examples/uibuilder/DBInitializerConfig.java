package hu.webvalto.examples.uibuilder;

import hu.webvalto.examples.uibuilder.generator.StudentGenerator;
import hu.webvalto.examples.uibuilder.repositories.StudentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
public class DBInitializerConfig {

    private final StudentRepo studentRepo;
    private final StudentGenerator studentGenerator;

    @Autowired
    public DBInitializerConfig(StudentRepo studentRepo, StudentGenerator studentGenerator) {
        this.studentRepo = studentRepo;
        this.studentGenerator = studentGenerator;
    }

    @PostConstruct
    public void initDB() {
        studentRepo.saveAll(studentGenerator.generateStudents(10_000));
    }

}
