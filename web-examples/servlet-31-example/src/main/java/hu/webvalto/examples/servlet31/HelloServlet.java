package hu.webvalto.examples.servlet31;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Optional;

@WebServlet(urlPatterns = {"/hello", "/hello/*"})
public class HelloServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = Arrays.stream(Optional.ofNullable(req.getCookies()).orElse(new Cookie[0]))
            .filter(cookie -> cookie.getName().equals("name"))
            .map(Cookie::getValue)
            .findAny()
            .orElse("Látogató");
        writeHello(resp, name);
    }

    private void writeHello(HttpServletResponse resp, String name) throws IOException {
        resp.setContentType("text/html");
        resp.getWriter().format(
            "<html><head><meta charset=\"UTF-8\"></head><body><h1>Hello %s</h1></body></html>",
            name
        );
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        Cookie cookie = new Cookie("name", name);
        cookie.setMaxAge(60);
        resp.addCookie(cookie);
        writeHello(resp, name);
    }
}
